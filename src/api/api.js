const { ApiPath } = require('../common');
const {
  userService,
  betService,
  transactionService,
  eventService
} = require('../services/services');
const { initUser } = require('./user/user.api');
const { initHealth } = require('./health/health.api');
const { initTransaction } = require('./transaction/transaction.api');
const { initEvent } = require('./event/event.api');
const { initBet } = require('./bet/bet.api');
const { initStats } = require('./stats/stats.api');

const initApi = Router => {
  const apiRouter = Router();

  apiRouter.use(ApiPath.HEALTH, initHealth(Router));

  apiRouter.use(ApiPath.STATS, initStats(Router));

  apiRouter.use(
    ApiPath.USER,
    initUser(Router, {
      user: userService
    })
  );

  apiRouter.use(
    ApiPath.TRANSACTION,
    initTransaction(Router, {
      transaction: transactionService
    })
  );

  apiRouter.use(
    ApiPath.EVENT,
    initEvent(Router, {
      event: eventService
    })
  );

  apiRouter.use(
    ApiPath.BET,
    initBet(Router, {
      bet: betService
    })
  );

  return apiRouter;
};

module.exports = { initApi };
