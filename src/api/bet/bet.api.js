const {
  BetApiPath,
  ValidationSchemas,
  ReqTypes,
  SubscribersName
} = require('../../common');
const { statEmitter } = require('../../helpers');
const { jwtMiddleware, dataValidationMiddleware } = require('../../middleware');

const initBet = (Router, services) => {
  const { bet: betService } = services;
  const router = Router();

  router.post(
    BetApiPath.ROOT,
    jwtMiddleware,
    dataValidationMiddleware(ValidationSchemas.betFieldsSchema, ReqTypes.BODY),
    (req, res, next) =>
      betService
        .create(req.userId, req.body)
        .then(data => {
          statEmitter.emit(SubscribersName.NEW_BET);
          res.send(data);
        })
        .catch(next)
  );

  return router;
};

module.exports = {
  initBet
};
