const {
  EventApiPath,
  ValidationSchemas,
  ReqTypes,
  SubscribersName
} = require('../../common');
const { statEmitter } = require('../../helpers');
const {
  dataValidationMiddleware,
  jwtAdminMiddleware
} = require('../../middleware');

const initEvent = (Router, services) => {
  const { event: eventService } = services;
  const router = Router();

  router
    .post(
      EventApiPath.ROOT,
      jwtAdminMiddleware,
      dataValidationMiddleware(
        ValidationSchemas.eventFieldsSchema,
        ReqTypes.BODY
      ),
      (req, res, next) =>
        eventService
          .create(req.body)
          .then(data => {
            statEmitter.emit(SubscribersName.NEW_EVENT);
            res.send(data);
          })
          .catch(next)
    )
    .put(
      EventApiPath.$ID,
      jwtAdminMiddleware,
      dataValidationMiddleware(
        ValidationSchemas.eventPartialSchema,
        ReqTypes.BODY
      ),
      (req, res, next) =>
        eventService
          .update(req.params.id, req.body)
          .then(data => res.send(data))
          .catch(next)
    );

  return router;
};

module.exports = {
  initEvent
};
