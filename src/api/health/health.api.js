const { HealthApiPath } = require('../../common');

const initHealth = Router => {
  const router = Router();

  router.get(HealthApiPath.ROOT, (_req, res) => {
    res.send('Hello World!');
  });

  return router;
};

module.exports = {
  initHealth
};
