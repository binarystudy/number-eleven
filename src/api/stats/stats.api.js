const { StatsApiPath, stats } = require('../../common');
const { jwtAdminMiddleware } = require('../../middleware');

const initStats = Router => {
  const router = Router();

  router.get(StatsApiPath.ROOT, jwtAdminMiddleware, (_req, res) => {
    res.send(stats);
  });

  return router;
};

module.exports = {
  initStats
};
