const {
  TransactionApiPath,
  ValidationSchemas,
  ReqTypes
} = require('../../common');
const {
  dataValidationMiddleware,
  jwtAdminMiddleware
} = require('../../middleware');

const initTransaction = (Router, services) => {
  const { transaction: transactionService } = services;
  const router = Router();

  router.post(
    TransactionApiPath.ROOT,
    jwtAdminMiddleware,
    dataValidationMiddleware(
      ValidationSchemas.transactionFieldsSchema,
      ReqTypes.BODY
    ),
    (req, res, next) =>
      transactionService
        .create(req.body)
        .then(data => res.send(data))
        .catch(next)
  );

  return router;
};

module.exports = {
  initTransaction
};
