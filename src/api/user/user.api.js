const {
  UserApiPath,
  ValidationSchemas,
  ReqTypes,
  SubscribersName
} = require('../../common');
const { statEmitter } = require('../../helpers');
const {
  dataValidationMiddleware,
  jwtUserMiddleware
} = require('../../middleware');

const initUser = (Router, services) => {
  const { user: userService } = services;
  const router = Router();

  router
    .get(
      UserApiPath.$ID,
      dataValidationMiddleware(ValidationSchemas.userIdSchema, ReqTypes.PARAMS),
      (req, res, next) =>
        userService
          .getUserById(req.params.id)
          .then(data => res.send(data))
          .catch(next)
    )
    .post(
      UserApiPath.ROOT,
      dataValidationMiddleware(
        ValidationSchemas.userFieldsSchema,
        ReqTypes.BODY
      ),
      (req, res, next) =>
        userService
          .register(req.body)
          .then(data => {
            statEmitter.emit(SubscribersName.NEW_USER);
            res.send(data);
          })
          .catch(next)
    )
    .put(
      UserApiPath.$ID,
      jwtUserMiddleware,
      dataValidationMiddleware(
        ValidationSchemas.userPartialFieldsSchema,
        ReqTypes.BODY
      ),
      (req, res, next) =>
        userService
          .update(req.userId, req.body)
          .then(data => res.send(data))
          .catch(next)
    );

  return router;
};

module.exports = {
  initUser
};
