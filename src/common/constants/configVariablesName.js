const ConfigVariablesName = [
  'APP_PORT',
  'DATABASE_PORT',
  'DATABASE_HOST',
  'DATABASE_NAME',
  'DATABASE_USER',
  'DATABASE_ACCESS_KEY',
  'DATABASE_CLIENT',
  'JWT_SECRET'
];

module.exports = {
  ConfigVariablesName
};
