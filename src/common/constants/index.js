const configure = require('./configVariablesName');
const subscribers = require('./subscribersName');

module.exports = {
  ...configure,
  ...subscribers
};
