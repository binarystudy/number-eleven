const stats = {
  totalUsers: 3,
  totalBets: 1,
  totalEvents: 1
};

const SubscribersName = {
  NEW_USER: 'newUser',
  NEW_BET: 'newBet',
  NEW_EVENT: 'newEvent'
};

module.exports = {
  stats,
  SubscribersName
};
