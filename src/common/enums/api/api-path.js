const ApiPath = {
  USER: '/users',
  HEALTH: '/health',
  TRANSACTION: '/transactions',
  EVENT: '/events',
  BET: '/bets',
  STATS: '/stats'
};

module.exports = {
  ApiPath
};
