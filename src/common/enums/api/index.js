const apiPath = require('./api-path');
const betApi = require('./bet-api-path');
const eventApi = require('./event-api-path');
const healthApi = require('./health-api.path');
const statsApi = require('./stats-api-path');
const transApi = require('./transaction-api-path');
const userApi = require('./user-api-path');

module.exports = {
  ...apiPath,
  ...betApi,
  ...eventApi,
  ...healthApi,
  ...statsApi,
  ...transApi,
  ...userApi
};
