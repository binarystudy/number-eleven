const UserApiPath = {
  ROOT: '/',
  $ID: '/:id'
};

module.exports = {
  UserApiPath
};
