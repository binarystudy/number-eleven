const { config } = require('dotenv');

config();

const configVariables = process.env;

const {
  APP_PORT,
  DATABASE_PORT,
  DATABASE_HOST,
  DATABASE_NAME,
  DATABASE_USER,
  DATABASE_ACCESS_KEY,
  DATABASE_CLIENT,
  JWT_SECRET
} = configVariables;

const ENV = {
  APP: {
    PORT: APP_PORT,
    API_PATH: ''
  },
  DB: {
    DATABASE: DATABASE_NAME,
    USERNAME: DATABASE_USER,
    PASSWORD: DATABASE_ACCESS_KEY,
    HOST: DATABASE_HOST,
    PORT: DATABASE_PORT,
    CLIENT: DATABASE_CLIENT
  },
  JWT: {
    SECRET: JWT_SECRET
  }
};

module.exports = {
  ENV,
  configVariables
};
