const DbName = {
  USER: 'user',
  ODDS: 'odds',
  TRANSACTION: 'transaction',
  EVENT: 'event',
  BET: 'bet'
};

module.exports = {
  DbName
};
