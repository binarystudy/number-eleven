const app = require('./app/env.enum');
const api = require('./api');
const db = require('./db/dbName');
const validation = require('./validationSchemas');

module.exports = {
  ...app,
  ...api,
  ...db,
  ...validation
};
