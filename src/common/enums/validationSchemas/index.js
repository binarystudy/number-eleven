const reqTypes = require('./reqTypes');
const schemas = require('./validationSchemas');

module.exports = {
  ...reqTypes,
  ...schemas
};
