const ReqTypes = {
  BODY: 'body',
  PARAMS: 'params'
};

module.exports = {
  ReqTypes
};
