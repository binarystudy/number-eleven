const ValidationSchemas = {
  userIdSchema: 'userIdSchema',
  userFieldsSchema: 'userFieldsSchema',
  userPartialFieldsSchema: 'userPartialFieldsSchema',
  transactionFieldsSchema: 'transactionFieldsSchema',
  eventFieldsSchema: 'eventFieldsSchema',
  betFieldsSchema: 'betFieldsSchema',
  eventPartialSchema: 'eventPartialSchema'
};

module.exports = {
  ValidationSchemas
};
