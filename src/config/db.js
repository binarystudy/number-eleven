const { ENV } = require('../common/enums/app/env.enum');

const {
  DATABASE: database,
  USERNAME: user,
  PASSWORD: password,
  HOST: host,
  PORT: port,
  CLIENT: client
} = ENV.DB;

const dbConfig = {
  client,
  connection: {
    port,
    host,
    database,
    user,
    password
  },
  migrations: {
    directory: './db/migrations',
    tableName: 'knex_migrations'
  },
  seeds: {
    directory: './db/seeds'
  },
  pool: {
    min: 0,
    max: 20
  }
};

module.exports = {
  dbConfig
};
