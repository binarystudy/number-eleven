const knex = require('knex');
const log = require('npmlog');
const { dbConfig } = require('../../config/db');

const db = knex(dbConfig);

db.raw('select 1+1 as result')
  .then(function () {
    log.info('Db connecting');
  })
  .catch(() => {
    log.error('No db connection');
    process.exit(1);
  });

module.exports = db;
