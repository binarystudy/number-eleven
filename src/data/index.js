const repositories = require('./repositories/repositories');

module.exports = {
  ...repositories
};
