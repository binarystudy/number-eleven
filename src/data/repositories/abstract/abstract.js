const db = require('../../db/connection');

class Abstract {
  constructor({ model, modelName }) {
    this.model = model;
    this.modelName = modelName;
  }

  async getById(id) {
    return await this.model(this.modelName).where({ id }).returning('*');
  }

  async create(data) {
    return await this.model(this.modelName).insert(data).returning('*');
  }

  async updateById(id, data) {
    const result = await this.model(this.modelName)
      .where({ id })
      .update(data)
      .returning('*');
    return result;
  }
}

module.exports = {
  Abstract
};
