const { Abstract } = require('../abstract/abstract');

class Bet extends Abstract {
  async getByEventWin(event_id, win) {
    return await this.model(this.modelName).where({ event_id, win });
  }
}

module.exports = {
  Bet
};
