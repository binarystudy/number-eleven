const { Abstract } = require('../abstract/abstract');

class Event extends Abstract {}

module.exports = {
  Event
};
