const { DbName } = require('../../common/enums/db/dbName');
const db = require('../db/connection');
const { User } = require('./user/user');
const { Transaction } = require('./transaction/transaction');
const { Odds } = require('./odds/odds');
const { Event } = require('./event/event');
const { Bet } = require('./bet/bet');

const user = new User({
  model: db,
  modelName: DbName.USER
});

const transaction = new Transaction({
  model: db,
  modelName: DbName.TRANSACTION
});

const odds = new Odds({
  model: db,
  modelName: DbName.ODDS
});

const event = new Event({
  model: db,
  modelName: DbName.EVENT
});

const bet = new Bet({
  model: db,
  modelName: DbName.BET
});

module.exports = { user, transaction, odds, event, bet };
