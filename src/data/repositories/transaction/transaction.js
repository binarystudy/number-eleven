const { Abstract } = require('../abstract/abstract');

class Transaction extends Abstract {}

module.exports = {
  Transaction
};
