const log = require('npmlog');
const { configVariables } = require('../../common');
const { ConfigVariablesName } = require('../../common/constants');

const checkConfig = () => {
  ConfigVariablesName.forEach(variable => {
    if (!configVariables[variable]) {
      log.error('Configuration', `is missing %j parameter!`, variable);
      process.exit(1);
    }
  });
};

module.exports = {
  checkConfig
};
