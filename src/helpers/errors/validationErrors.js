const { CustomError } = require('./error');

class ValidationError extends CustomError {
  constructor({ message, status = 400 }) {
    super(message);
    this.status = status;
  }
}

module.exports = {
  ValidationError
};
