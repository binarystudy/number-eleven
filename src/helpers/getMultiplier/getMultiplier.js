const getMultiplier = (prediction, { home_win, away_win, draw }) => {
  switch (prediction) {
    case 'w1':
      return home_win;
    case 'w2':
      return away_win;
    case 'x':
      return draw;
  }
};

module.exports = {
  getMultiplier
};
