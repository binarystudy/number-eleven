const getResultBet = score => {
  const [w1, w2] = score.split(':');
  if (+w1 > +w2) {
    return 'w1';
  }
  return +w2 > +w1 ? 'w2' : 'x';
};

module.exports = {
  getResultBet
};
