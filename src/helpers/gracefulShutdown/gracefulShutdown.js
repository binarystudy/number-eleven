const log = require('npmlog');
const db = require('../../data/db/connection');

const gracefulShutdown = server => {
  process.on('SIGINT', () => {
    log.info('Closing http server.');
    server.close(() => {
      log.info('Http server closed.');
      db.destroy();
      log.info('Db connection closed.');
      process.exit(0);
    });
  });
};

module.exports = {
  gracefulShutdown
};
