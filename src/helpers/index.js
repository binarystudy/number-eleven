const checkConfig = require('./checkConfig/checkConfig');
const error = require('./errors');
const getMultiplier = require('./getMultiplier/getMultiplier');
const getResultBet = require('./getResultBet/getResultBet');
const statsEmitter = require('./statsEmitter/statsEmitter');
const toCamelCase = require('./toCamelCase/toCamelCase');
const token = require('./token');
const gracefulShutdown = require('./gracefulShutdown/gracefulShutdown');

module.exports = {
  ...checkConfig,
  ...error,
  ...getMultiplier,
  ...getResultBet,
  ...statsEmitter,
  ...toCamelCase,
  ...token,
  ...gracefulShutdown
};
