const ee = require('events');
const {
  SubscribersName,
  stats
} = require('../../common/constants/subscribersName');

const statEmitter = new ee();

const subscribeToStatEmitter = () => {
  statEmitter.on(SubscribersName.NEW_USER, () => {
    stats.totalUsers += 1;
  });
  statEmitter.on(SubscribersName.NEW_BET, () => {
    stats.totalBets += 1;
  });
  statEmitter.on(SubscribersName.NEW_EVENT, () => {
    stats.totalEvents += 1;
  });
};

module.exports = {
  statEmitter,
  subscribeToStatEmitter
};
