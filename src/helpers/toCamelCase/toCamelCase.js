const toCamelCase = obj => {
  const array = Object.entries(obj).map(([key, value]) => {
    const index = key.indexOf('_');
    if (index === -1) {
      return [key, value];
    }
    const newKey =
      key.slice(0, index) +
      key.slice(index + 1, index + 2).toUpperCase() +
      key.slice(index + 2);
    return [newKey, value];
  });
  return Object.fromEntries(array);
};

module.exports = {
  toCamelCase
};
