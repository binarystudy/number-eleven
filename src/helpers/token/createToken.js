const jwt = require('jsonwebtoken');
const { ENV } = require('../../common/enums/app/env.enum');

const createToken = data => jwt.sign(data, ENV.JWT.SECRET);

module.exports = { createToken };
