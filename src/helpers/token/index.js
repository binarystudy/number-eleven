const verify = require('./verifyToken');
const create = require('./createToken');

module.exports = {
  ...create,
  ...verify
};
