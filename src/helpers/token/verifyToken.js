const jwt = require('jsonwebtoken');
const { ENV } = require('../../common/enums/app/env.enum');

const verifyToken = token => jwt.verify(token, ENV.JWT.SECRET);

module.exports = { verifyToken };
