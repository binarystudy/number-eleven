const express = require('express');
const log = require('npmlog');
const { ENV } = require('./common');
const {
  errorHandler: errorHandlerMiddleware,
  loggerMiddleware
} = require('./middleware');
const { initApi } = require('./api/api');
const { subscribeToStatEmitter, gracefulShutdown } = require('./helpers');
require('./data/db/connection');
const { checkConfig } = require('./helpers');

checkConfig();

const app = express();

app.use(express.json());

app.use(loggerMiddleware);

app.use(ENV.APP.API_PATH, initApi(express.Router));

app.use(errorHandlerMiddleware);

const server = app.listen(ENV.APP.PORT, () => {
  subscribeToStatEmitter();
  log.info(`Server listening on port ${ENV.APP.PORT}!`);
});

gracefulShutdown(server);

module.exports = { app };
