const { ValidationError } = require('../../helpers/errors/validationErrors');
const { userFieldsSchema } = require('./userFieldsSchema');
const { userIdSchema } = require('./userIdSchema');
const { userPartialFieldsSchema } = require('./userPartialFieldsSchema');
const { transactionFieldsSchema } = require('./transactionFieldsSchema');
const { eventFieldsSchema } = require('./eventFieldsSchema');
const { betFieldsSchema } = require('./betFieldsSchema');
const { eventPartialSchema } = require('./eventPartialSchema');

const schemas = {
  userFieldsSchema,
  userIdSchema,
  userPartialFieldsSchema,
  transactionFieldsSchema,
  eventFieldsSchema,
  betFieldsSchema,
  eventPartialSchema
};

const dataValidationMiddleware = (schemaType, reqType) => (req, _res, next) => {
  const schema = schemas[schemaType];
  const isValidResult = schema.validate(req[reqType]);
  if (isValidResult.error) {
    throw new ValidationError({
      message: isValidResult.error.details[0].message
    });
  }
  next();
};

module.exports = {
  dataValidationMiddleware
};
