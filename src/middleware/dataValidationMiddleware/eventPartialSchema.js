const joi = require('joi');

const eventPartialSchema = joi
  .object({
    score: joi.string().required()
  })
  .required();

module.exports = {
  eventPartialSchema
};
