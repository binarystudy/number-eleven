const joi = require('joi');

const userIdSchema = joi
  .object({
    id: joi.string().uuid()
  })
  .required();

module.exports = {
  userIdSchema
};
