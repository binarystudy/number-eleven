const errorHandler = (err, _req, res, next) => {
  if (res.headersSent) {
    next(err);
  } else {
    let status = 500;
    let error = 'Internal Server Error';
    if (err?.name === 'ValidationError') {
      status = err.status;
      error = err.message;
    }
    if (err?.code === '23505') {
      status = 400;
      error = err?.detail;
    }
    res.status(status).send({ status, error });
  }
};

module.exports = {
  errorHandler
};
