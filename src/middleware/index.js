const dataValidationMiddleware = require('./dataValidationMiddleware/dataValidation');
const error = require('./error/error-handler.middleware');
const jwt = require('./jwtMiddleware/jwtMiddleware');
const logger = require('./loggers/loggersMiddleware');

module.exports = {
  ...dataValidationMiddleware,
  ...error,
  ...jwt,
  ...logger
};
