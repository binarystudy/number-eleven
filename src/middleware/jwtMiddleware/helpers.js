const { ValidationError } = require('../../helpers/errors/validationErrors');
const { verifyToken } = require('../../helpers/token/verifyToken');

const jwtHelpers = headers => {
  let token = headers.authorization;
  let tokenPayload;
  if (!token) {
    throw new ValidationError({ message: 'Not Authorized', status: 401 });
  }
  token = token.replace('Bearer ', '');
  try {
    tokenPayload = verifyToken(token);
  } catch (err) {
    throw new ValidationError({ message: 'Not Authorized', status: 401 });
  }
  return tokenPayload;
};

module.exports = {
  jwtHelpers
};
