const { ValidationError } = require('../../helpers/errors/validationErrors');
const { jwtHelpers } = require('./helpers');

const jwtMiddleware = (req, _res, next) => {
  const tokenPayload = jwtHelpers(req.headers);
  req.userId = tokenPayload.id;
  next();
};

const jwtUserMiddleware = (req, _res, next) => {
  const tokenPayload = jwtHelpers(req.headers);
  if (req?.params?.id !== tokenPayload.id) {
    throw new ValidationError({ message: 'UserId mismatch', status: 401 });
  }
  req.userId = tokenPayload.id;
  next();
};

const jwtAdminMiddleware = (req, _res, next) => {
  const tokenPayload = jwtHelpers(req.headers);
  if (tokenPayload.type !== 'admin') {
    throw new ValidationError({ message: 'Not Authorized', status: 401 });
  }
  req.userId = tokenPayload.id;
  next();
};

module.exports = {
  jwtMiddleware,
  jwtUserMiddleware,
  jwtAdminMiddleware
};
