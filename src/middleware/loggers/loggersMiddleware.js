const log = require('npmlog');

const loggerMiddleware = (req, res, next) => {
  log.info(
    'log In',
    `url: ${req.url}, method: ${
      req.method
    }, time: ${new Date().toLocaleString()}`
  );
  req.logger = {
    timestamp: Date.now()
  };
  const oldSend = res.send;
  const { url } = req;
  res.send = (...data) => {
    log.info(
      'log out',
      `url: ${url}, method: ${req.method}, deltaTime: ${
        Date.now() - req.logger.timestamp
      } ms, response: ${JSON.stringify(data)}`
    );
    res.send = oldSend;
    oldSend.apply(res, data);
  };
  next();
};

module.exports = {
  loggerMiddleware
};
