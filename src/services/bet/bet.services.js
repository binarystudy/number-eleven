const {
  ValidationError,
  getMultiplier,
  toCamelCase
} = require('../../helpers');

class Bet {
  constructor({
    oddsRepository,
    eventRepository,
    betRepository,
    userRepository
  }) {
    this._oddsRepository = oddsRepository;
    this._eventRepository = eventRepository;
    this._betRepository = betRepository;
    this._userRepository = userRepository;
  }

  async create(userId, data) {
    const { betAmount: bet_amount, eventId: event_id, ...bet } = data;
    const [user] = await this._userRepository.getById(userId);
    if (!user) {
      throw new ValidationError({ message: 'User does not exist' });
    }
    if (+user.balance < +bet_amount) {
      throw new ValidationError({ message: 'Not enough balance' });
    }
    const [event] = await this._eventRepository.getById(event_id);
    if (!event) {
      throw new ValidationError({ status: 404, message: 'Event not found' });
    }
    const [odds] = await this._oddsRepository.getById(event.odds_id);
    if (!odds) {
      throw new ValidationError({ status: 400, message: 'Odds not found' });
    }
    const multiplier = getMultiplier(data.prediction, odds);
    const [newBet] = await this._betRepository.create({
      ...bet,
      bet_amount,
      multiplier,
      event_id: event.id,
      user_id: userId
    });
    const currentBalance = user.balance - bet_amount;
    await this._userRepository.updateById(userId, {
      balance: currentBalance
    });
    return {
      ...toCamelCase(newBet),
      currentBalance
    };
  }
}

module.exports = {
  Bet
};
