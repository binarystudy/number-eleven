const { toCamelCase, getResultBet } = require('../../helpers');

class Event {
  constructor({
    oddsRepository,
    eventRepository,
    betRepository,
    userRepository
  }) {
    this._oddsRepository = oddsRepository;
    this._eventRepository = eventRepository;
    this._betRepository = betRepository;
    this._userRepository = userRepository;
  }

  async create(data) {
    const { odds, ...event } = data;
    const { awayWin, homeWin, ...restOdds } = odds;
    const [newOdds] = await this._oddsRepository.create({
      ...restOdds,
      home_win: homeWin,
      away_win: awayWin
    });
    const { awayTeam, homeTeam, startAt, ...restEvent } = event;
    const [newEvent] = await this._eventRepository.create({
      ...restEvent,
      away_team: awayTeam,
      home_team: homeTeam,
      start_at: startAt,
      odds_id: newOdds.id
    });
    return {
      ...toCamelCase(newEvent),
      odds: toCamelCase(newOdds)
    };
  }

  async update(eventId, data) {
    const bets = await this._betRepository.getByEventWin(eventId, null);
    const [event] = await this._eventRepository.updateById(eventId, {
      score: data.score
    });

    const result = getResultBet(data.score);
    for (const bet of bets) {
      if (bet.prediction === result) {
        const [user] = await this._userRepository.getById(bet.user_id);
        await this._betRepository.updateById(bet.id, { win: true });
        await this._userRepository.updateById(bet.user_id, {
          balance: user.balance + bet.bet_amount * bet.multiplier
        });
      } else {
        await this._betRepository.updateById(bet.id, { win: false });
      }
    }
    return toCamelCase(event);
  }
}

module.exports = {
  Event
};
