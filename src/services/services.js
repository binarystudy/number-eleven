const {
  user: userRepository,
  odds: oddsRepository,
  event: eventRepository,
  bet: betRepository,
  transaction: transactionRepository
} = require('../data');
const { User } = require('./user/user.service');
const { Transaction } = require('./transaction/transaction.services');
const { Event } = require('./event/event.services');
const { Bet } = require('./bet/bet.services');

const userService = new User({
  userRepository
});

const betService = new Bet({
  userRepository,
  oddsRepository,
  eventRepository,
  betRepository
});

const transactionService = new Transaction({
  userRepository,
  transactionRepository
});

const eventService = new Event({
  oddsRepository,
  eventRepository,
  betRepository,
  userRepository
});

module.exports = { userService, betService, transactionService, eventService };
