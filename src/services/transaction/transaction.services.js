const { ValidationError, toCamelCase } = require('../../helpers');

class Transaction {
  constructor({ transactionRepository, userRepository }) {
    this._transactionRepository = transactionRepository;
    this._userRepository = userRepository;
  }

  async create({ userId, ...data }) {
    const [user] = await this._userRepository.getById(userId);
    if (!user) {
      throw new ValidationError({ message: 'User does not exist' });
    }
    const { cardNumber, ...transaction } = data;
    const [newTransaction] = await this._transactionRepository.create({
      ...transaction,
      card_number: cardNumber,
      user_id: userId
    });

    const currentBalance = data.amount + user.balance;
    await this._userRepository.updateById(userId, {
      balance: currentBalance
    });
    return {
      ...toCamelCase(newTransaction),
      currentBalance
    };
  }
}

module.exports = {
  Transaction
};
