const { ValidationError, createToken } = require('../../helpers');

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const [user] = await this._userRepository.getById(id);
    if (!user) {
      throw new ValidationError({ message: 'User not found', status: 404 });
    }
    return user;
  }

  async register(data) {
    const [user] = await this._userRepository.create({ ...data, balance: 0 });
    const { created_at, updated_at, ...restUser } = user;
    return {
      ...restUser,
      createdAt: created_at,
      updatedAt: updated_at,
      accessToken: createToken({ id: user.id, type: user.type })
    };
  }

  async update(id, data) {
    const [user] = await this._userRepository.updateById(id, data);
    return user;
  }
}

module.exports = {
  User
};
